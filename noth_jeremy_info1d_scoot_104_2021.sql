-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Dim 18 Avril 2021 à 08:53
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `noth_jeremy_info1d_scoot_104_2021`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_contact`
--

CREATE TABLE `t_contact` (
  `ID_contact` int(11) NOT NULL,
  `Contact_personne` varchar(10) NOT NULL,
  `Contact_parent` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `t_contact_parent`
--

CREATE TABLE `t_contact_parent` (
  `ID_contact_parent` int(11) NOT NULL,
  `Telephone_parent` varchar(10) NOT NULL,
  `Mail_parent` varchar(42) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `t_contact_scoot`
--

CREATE TABLE `t_contact_scoot` (
  `ID_contact_scoot` int(11) NOT NULL,
  `Telephone_scoot` varchar(42) NOT NULL,
  `Mail_scoot` varchar(42) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `t_materiel`
--

CREATE TABLE `t_materiel` (
  `ID_materiel` int(11) NOT NULL,
  `Nom_materiel` varchar(50) NOT NULL,
  `Nombre_personne_materiel` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `t_personne`
--

CREATE TABLE `t_personne` (
  `ID_personne` int(11) NOT NULL,
  `Prenom_personne` varchar(42) NOT NULL,
  `Nom_personne` varchar(42) NOT NULL,
  `Date_naissance_personne` date NOT NULL,
  `Genre_personne` varchar(32) NOT NULL,
  `presence_personne` tinyint(1) NOT NULL,
  `fk_contact` int(11) NOT NULL,
  `fk_materiel` int(11) NOT NULL,
  `fk_contact_scoot` int(11) NOT NULL,
  `fk_contact_parent` int(11) NOT NULL,
  `fk_presence` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `t_presence`
--

CREATE TABLE `t_presence` (
  `ID_presence` int(11) NOT NULL,
  `statut_presence` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_contact`
--
ALTER TABLE `t_contact`
  ADD PRIMARY KEY (`ID_contact`);

--
-- Index pour la table `t_contact_parent`
--
ALTER TABLE `t_contact_parent`
  ADD PRIMARY KEY (`ID_contact_parent`);

--
-- Index pour la table `t_contact_scoot`
--
ALTER TABLE `t_contact_scoot`
  ADD PRIMARY KEY (`ID_contact_scoot`);

--
-- Index pour la table `t_materiel`
--
ALTER TABLE `t_materiel`
  ADD PRIMARY KEY (`ID_materiel`);

--
-- Index pour la table `t_personne`
--
ALTER TABLE `t_personne`
  ADD PRIMARY KEY (`ID_personne`),
  ADD KEY `fk_contact` (`fk_contact`),
  ADD KEY `fk_materiel` (`fk_materiel`),
  ADD KEY `fk_contact_scoot` (`fk_contact_scoot`),
  ADD KEY `fk_contact_parent` (`fk_contact_parent`),
  ADD KEY `fk_presence` (`fk_presence`);

--
-- Index pour la table `t_presence`
--
ALTER TABLE `t_presence`
  ADD PRIMARY KEY (`ID_presence`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_contact`
--
ALTER TABLE `t_contact`
  MODIFY `ID_contact` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_contact_parent`
--
ALTER TABLE `t_contact_parent`
  MODIFY `ID_contact_parent` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_contact_scoot`
--
ALTER TABLE `t_contact_scoot`
  MODIFY `ID_contact_scoot` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_materiel`
--
ALTER TABLE `t_materiel`
  MODIFY `ID_materiel` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_personne`
--
ALTER TABLE `t_personne`
  MODIFY `ID_personne` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_presence`
--
ALTER TABLE `t_presence`
  MODIFY `ID_presence` int(11) NOT NULL AUTO_INCREMENT;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_personne`
--
ALTER TABLE `t_personne`
  ADD CONSTRAINT `t_personne_ibfk_1` FOREIGN KEY (`fk_contact`) REFERENCES `t_contact` (`ID_contact`),
  ADD CONSTRAINT `t_personne_ibfk_2` FOREIGN KEY (`fk_materiel`) REFERENCES `t_materiel` (`ID_materiel`),
  ADD CONSTRAINT `t_personne_ibfk_3` FOREIGN KEY (`fk_contact_scoot`) REFERENCES `t_contact_scoot` (`ID_contact_scoot`),
  ADD CONSTRAINT `t_personne_ibfk_4` FOREIGN KEY (`fk_contact_parent`) REFERENCES `t_contact_parent` (`ID_contact_parent`),
  ADD CONSTRAINT `t_personne_ibfk_5` FOREIGN KEY (`fk_presence`) REFERENCES `t_presence` (`ID_presence`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
